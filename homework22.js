"use strict";

// Упражнение 1

const arr1 = [1, 2, 10, 5];
const arr2 = ["a", {},3,3, -2];

console.log(getSumm(arr2));

function getSumm(arr) {
    let summ = 0;
    for (let i = 0; i < arr.length; i++) {
        const current = arr[i];

        if (typeof current === 'number') {
            summ += current;
        }
    }

    return summ;
}

// Упражнение 2

// data.js

// Упражнение 3

let cart = [4884];

function addToCart(productId) {
    let hasInCart = cart.includes(productId);

    if (hasInCart) return;

    cart.push(productId);
}

function removeFromCart(productId) {
    cart = cart.filter(function (id) {
        return id !== productId;
    });
}

addToCart(3456);

console.log(cart);

removeFromCart(4884);

console.log(cart);