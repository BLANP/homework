"use strict";

// Упражнение 1

let a = '$100';
let b = '300$';

let summ = (+ a.slice(1)) + (+ parseInt(b)); // Ваше решение

console.log(summ); // Должно быть 400

// Упражнение 2

let message = ' привет, медвед      ';
message = message.trim()[0].toUpperCase() + message.slice(1); // Решение должно быть написано тут
console.log(message); // “Привет, медвед”

// Упражнение 3

let age = +prompt('Сколько Вам лет?');

if (age >= 0 && age <= 3) {
    alert(`Вам ${age} лет и Вы младенец`);
} else if (age >= 4 && age <= 11) {
    alert(`Вам ${age} лет и Вы ребёнок`);
} else if (age >= 12 && age <= 18) {
    alert(`Вам ${age} лет и Вы подросток`);
} else if (age >= 19 && age <= 40) {
    alert(`Вам ${age} лет и Вы познаёте жизнь`);
} else if (age >= 41 && age <= 80) {
    alert(`Вам ${age} лет и Вы познали жизнь`);
} else if (age >= 81) {
    alert(`Вам ${age} лет и Вы долгожитель`);
}

// Упражнение 4

let message1 ='Я работаю со строками как профессионал!';

let count = message1.split(` `).length; // Ваше решение

console.log(count); // Должно быть 6
