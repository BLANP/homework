"use strict";

// Упражнение 1

for (let c = 0; c <= 20; c++) {
    if (c % 2 === 0) {
        console.log(c);
    }
}

// Упражнение 2

let userNumb1 = +prompt(`Введите первое число`);
let userNumb2 = +prompt(`Введите второе число`);
let userNumb3 = +prompt(`Введите третье число`);

let userSumm = userNumb1 + userNumb2 + userNumb3;

if (isNaN(userSumm)) {
    alert('Ошибка, Вы ввели не число');
} else {
    alert(userSumm)
}

// Упражнение 3

function getNameOfMonth(n) {
    if (n === 0) return 'Январь';
    if (n === 1) return 'Февраль';
    if (n === 2) return 'Март';
    if (n === 3) return 'Апрель';
    if (n === 4) return 'Май';
    if (n === 5) return 'Июнь';
    if (n === 6) return 'Июль';
    if (n === 7) return 'Август';
    if (n === 8) return 'Сентябрь';
    if (n === 9) return 'Октябрь';
    if (n === 10) return 'Ноябрь';
    if (n === 11) return 'Декабрь';
}

for (let n = 0; n < 12; n++) {
    const month = getNameOfMonth(n);

    if (n === 9) continue;

    console.log(month);
}

// Упражнение 4

;(function () {
    const a = `IIFE — это функция, которая выполняется сразу же после того, как была определена. При помощи IIFE мы можем использовать одинаковые названия переменных, не боясь, что они случайно перезапишут значения переменных из чужих модулей, если мы не контролируем кодовую базу полностью сами.`
    console.log(a)
  })()
  