"use strict";

//& Упражнение 1

const seconds = +prompt('Введите количество секунд');

//* timer(seconds);

const timerPromise = new promiseTimer(seconds);

timerPromise
  .then(function (data) {
    let number = data;

    const interval = setInterval(() => {
      number -= 1;
      console.log('Осталось = ', number);

      if (number <= 0) {
        clearInterval(interval);

        setTimeout(() => {
          console.log('Время вышло!');
        }, 1000);
      }
    }, 1000);
  })
  .then(function(data) {
    console.log(data);
  })
  .catch(function (e) {
    console.log(e);
  })
  .finally(function () {
    console.log('finally');
  });

function promiseTimer(time) {
  return new Promise(function (resolve, reject) {
    if (!isNaN(time)) {
      resolve(time);
    } else {
      reject('Не число');
    }
  });
}

function timer(time) {
  let number = time;

  if (!isNaN(time)) {
    const interval = setInterval(() => {
      number -= 1;
      console.log('Осталось = ', number);

      if (number <= 0) {
        clearInterval(interval);

        setTimeout(() => {
          console.log('Время вышло!');
        }, 1000);
      }
    }, 1000);
  } else {
    console.log('Вы ввели не число!');
  }
}

//& Упражнение 2

const url = 'https://reqres.in/api/users';
const data = fetch(url);

const startDate = Date.now();

data
  .then(function (request) {
    return request.json();
  })
  .then(function (data) {
    console.log('Получили пользователей = ', data.data.length);
    data.data.forEach(function (elem) {
      console.log(`${elem.first_name} ${elem.last_name} (${elem.email})`);
    });

    console.log(Date.now() - startDate);
  })
  .catch(function (e) {
    console.log(e);
  });
